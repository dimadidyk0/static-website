module.exports = {
	env: {
		browser: true,
		es6: true
	},
	settings: {
		'import/core-modules': ['react']
	},
	extends: ['plugin:react/recommended', 'prettier', 'prettier/react', 'airbnb'],
	globals: {
		Atomics: 'readonly',
		SharedArrayBuffer: 'readonly'
	},
	parserOptions: {
		ecmaFeatures: {
			jsx: true
		},
		ecmaVersion: 2018,
		sourceType: 'module'
	},
	plugins: ['react'],
	rules: {
		'prettier/prettier': [
			'warn',
			{
				singleQuote: true,
				printWidth: 120,
				trailingComma: 'es5'
			}
		],
		'react/jsx-filename-extension': [
			1,
			{
				extensions: ['.js', '.jsx']
			}
		],
		'react/prop-types': 0,
		'no-unused-vars': [
			'error',
			{
				vars: 'local',
				args: 'none'
			}
		],
		'jsx-a11y/anchor-is-valid': [
			'error',
			{
				components: ['Link'],
				specialLink: ['to', 'hrefLeft', 'hrefRight'],
				aspects: ['noHref', 'invalidHref', 'preferButton']
			}
		]
	}
};
