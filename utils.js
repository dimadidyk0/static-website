
function handleImages(images) {
	if (!images || images.length === 0) {
		return [];
	}

	return images
		.sort()
		.map(({ url }) => ({ url }));
}


function buildImages(product) {
	const map = product.i200.map(el => el.name).sort((a, b) => {
		if (a > b) return 1;
		if (a < b) return -1;

		return 0;
	});

	return map.reduce((acc, el) => {
		Object.keys(acc).forEach(key => {
			acc[key].push(product[key].find(item => item.name === el));
		})
		return acc;
	}, {
		i200: [],
		i400: [],
		i640: [],
	})
}

function formatProductsForHomePage(products) {
	return products.map(({
		title,
		playerMinAge,
    playerMaxCount,
		shortDescription,
    _id,
    article,
    price,
    count,
    id,
    brandColor,
    maxDuration,
    minDuration,
		playerMinCount,

		...product
	}) => ({
		title,
		playerMinAge,
		playerMaxCount,
		shortDescription,
    _id,
    article,
    price,
    count,
    id,
    brandColor,
    maxDuration,
    minDuration,
		playerMinCount,

		images: handleImages(product.images || []),
		...buildImages(product),
	}))
}

function formatCategoriesForHomePage(categories) {
	return categories.map(({
		link, label, order, _id, id, title,
	}) => ({
		link, label, order, _id, id, title,
	}));
}

function formatCategory({ products: _, ...category }) {
	return {
		...category,
	}
}

function formatPagesForNav(pages) {
	return pages.map(({ _id, title }) => ({
		title,
		_id,
	}));
}

module.exports = {
	handleImages,
	buildImages,
	formatProductsForHomePage,
	formatCategoriesForHomePage,
	formatCategory,
	formatPagesForNav,
}