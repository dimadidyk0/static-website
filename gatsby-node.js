/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const fetch = require('isomorphic-fetch');

const API = 'https://calm-hamlet-90481.herokuapp.com';
const PRODUCTS_PATH = `${API}/products?_limit=0&active=true&count_gte=1&_sort=order:DESC`;
const CATEGORIES_PATH = `${API}/categories?_limit=0`;
const PAGES_PATH = `${API}/pages?_limit=0`;
const utils = require('./utils');

const { 
	buildImages,
	formatProductsForHomePage,
	formatCategoriesForHomePage,
	formatCategory,
	formatPagesForNav,
} = utils;

function getProducts() {
	return fetch(PRODUCTS_PATH).then(r => r.json());
}

function getCategories() {
	return fetch(CATEGORIES_PATH).then(r => r.json());
}

function getPages() {
	return fetch(PAGES_PATH).then(r => r.json());
}

exports.createPages = async ({ actions: { createPage } }) => {
	const allProducts = await getProducts();
	const categories = await getCategories();
	const pages = await getPages();

	// TODO: replace menu to constants for FE

	// Create a page that lists all Pokémon.
	createPage({
		path: `/`,
		component: require.resolve('./src/templates/Home/Home.js'),
		context: {
			products: formatProductsForHomePage(allProducts),
			categories: formatCategoriesForHomePage(categories),
			pages: formatPagesForNav(pages),
		}
	});

	// Create a page for category
	categories.forEach(category => {
		createPage({
			path: `/category/${category._id}/`,
			component: require.resolve('./src/templates/Category/Category.js'),
			context: {
				category: formatCategory(category),
				categories: formatCategoriesForHomePage(categories),
				pages: formatPagesForNav(pages),
				// TODO: use products from category
				products: formatProductsForHomePage(
					allProducts
						.filter(game => game.categories.find(({ _id }) => _id === category._id))
				),
			}
		});
	});

	allProducts.forEach(({ categories: _, ...product }) => {
		createPage({
			path: `/products/${product._id}/`,
			component: require.resolve('./src/templates/Product/Product.js'),
			context: {
				product: {
					...product,
					...buildImages(product),
				},
				categories: formatCategoriesForHomePage(categories),
				pages: formatPagesForNav(pages),
			}
		});
	});

	pages.forEach(page => {
		createPage({
			path: `/page/${page._id}/`,
			component: require.resolve('./src/templates/Page/Page.js'),
			context: {
				categories: formatCategoriesForHomePage(categories),
				page,
				pages: formatPagesForNav(pages),
			}
		});
	});
};
