import { combineReducers } from 'redux';

import { productsReducer as products } from './products';
import globals from './globals';

const rootReducer = combineReducers({
  products,
  globals,
});

export default rootReducer;
