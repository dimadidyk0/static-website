import { SET_GLOBALS_LANGUAGE } from '../constants';

const initialLanguage = 'RU';

const initialState = {
  language: initialLanguage,
  isMobile: false,
};

const globalsReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_GLOBALS_LANGUAGE:
      return {
        ...state,
        language: payload,
      };

    default:
      return state;
  }
};

export default globalsReducer;
