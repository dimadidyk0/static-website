import path from 'ramda/src/path';
import { createSelector } from 'reselect';

import { LOAD_PRODUCTS, START, SUCCESS, ERROR } from '../constants/actionTypes';

export const DEFAULT_ERROR_VALUE = null;
export const DEFAULT_DATA_VALUE = [];

// data should be taken by uniq key

// Example:
// key: ?category=top&minPrice=200
// state: {
//   '?category=top&minPrice=200': {
//     isLoading: false,
//     error: null,
//     data: [],
//   },
// };x

const initialState = {};

export const productsReducer = (state = initialState, { payload, type }) => {
  const key = path(['key'], payload);
  const data = path(['data'], payload);

  switch (type) {
    case `${LOAD_PRODUCTS}${START}`:
      return {
        ...state,
        [key]: {
          isLoading: true,
          error: DEFAULT_ERROR_VALUE,
          data: DEFAULT_DATA_VALUE,
        },
      };

    case `${LOAD_PRODUCTS}${SUCCESS}`:
      return {
        ...state,
        [key]: {
          isLoading: false,
          error: DEFAULT_ERROR_VALUE,
          data,
        },
      };

    case `${LOAD_PRODUCTS}${ERROR}`:
      return {
        ...state,
        [key]: {
          isLoading: false,
          error: data,
          data: DEFAULT_DATA_VALUE,
        },
      };

    default:
      return state;
  }
};

// selectors

export const getCategoryFromPropsSelector = (_, ownProps) => {
  return path(['match', 'params', 'category'], ownProps);
};

export const getProducts = state => {
  return path(['products'], state);
};

export const getProductsIsLoadingByCategoryIdSelector = createSelector(
  getCategoryFromPropsSelector,
  getProducts,
  (category, products) => path([category, 'isLoading'], products),
);

export const getProductsDataByCategoryIdSelector = createSelector(
  getCategoryFromPropsSelector,
  getProducts,
  (category, products) => path([category, 'data'], products),
);

export const getProductsErrorByCategoryIdSelector = createSelector(
  getCategoryFromPropsSelector,
  getProducts,
  (category, products) => path([category, 'error'], products),
);

// TODO: think about it in future
// export const getProductsIsLoadingByCategoryIdSelector = () => {
//   return createSelector(
//     getCategoryFromPropsSelector,
//     getProducts,
//     (category, products) => path([category, 'isLoading'], products),
//   );
// };

// export const getProductsDataByCategoryIdSelector = () => {
//   return createSelector(
//     getCategoryFromPropsSelector,
//     getProducts,
//     (category, products) => path([category, 'data'], products),
//   );
// };

// export const getProductsErrorByCategoryIdSelector = () => {
//   return createSelector(
//     getCategoryFromPropsSelector,
//     getProducts,
//     (category, products) => path([category, 'error'], products),
//   );
// };

export default productsReducer;
