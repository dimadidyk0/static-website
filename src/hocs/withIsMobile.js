import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default WrappedComponent => {
	class withIsMobile extends Component {
		componentDidMount() {
			this.setState({ isMobile: window.innerWidth <= 1024 });
		}

		render() {
			return (
				<WrappedComponent {...this.props} isMobile={this.state.isMobile} />
			);
		}
	}

	return withIsMobile;
};
