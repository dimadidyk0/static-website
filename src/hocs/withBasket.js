import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getBasket } from 'selectors';

export default WrappedComponent => {
  class withBasket extends Component {
    static propTypes = {
      basket: PropTypes.shape({}).isRequired,
    };

    render() {
      const { basket } = this.props;

      return (
        <WrappedComponent
          {...this.props}
          count={basket.count || 0}
          amount={basket.amount || 0}
        />
      );
    }
  }

  const mapStateToProps = (state, ownProps) => ({
    basket: getBasket(state, ownProps),
  });

  const mapDispatchToProps = null;

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withBasket);
};
