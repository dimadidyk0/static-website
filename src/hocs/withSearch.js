import React, { Component } from 'react';
import { debounce } from '../helpers/utils';
import { PRODUCTS_URL } from '../constants/API';
import PropTypes from 'prop-types';

export default WrappedComponent => {
	class withSearch extends Component {
		state = {
			inputValue: '',
			isFetching: false,
			products: [],
		};

		handleSearch = () => {
			const { inputValue } = this.state;

			if (!inputValue) {
				this.setState({ products: [] });

				return;
			}

			this.setState({ isFetching: true }, async () => {
				const products = await fetch(
					`${PRODUCTS_URL}?title_contains=${inputValue}&active=true&count_gte=1`
				).then(r => r.json());

				this.setState({
					products,
					isFetching: false
				});
			});
		};

		debouncedHandleSearch = debounce(this.handleSearch, 250);

		handleChange = e => {
			const { value } = e.target;
			this.setState({ inputValue: value }, () => {
				this.debouncedHandleSearch();
			});
		};

		handleClear = e => {
			e.preventDefault();
			this.setState({ inputValue: '', products: [] });
		};

		render() {
			const { products, isFetching, inputValue } = this.state;

			return (
				<WrappedComponent
					onInputChange={this.handleChange}
					onInputClear={this.handleClear}
					products={products}
					isProductsFetching={isFetching}
					inputValue={inputValue}
					{...this.props}
				/>
			);
		}
	}

	return withSearch;
};
