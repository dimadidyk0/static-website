import React from 'react';
import { Link } from 'gatsby';
import Container from '../Container/Container';
import Logo from '../Logo/Logo';
import { phone, parsedPhone, mail } from '../../helpers';
import s from './Footer.module.css';

function Column({ title, children }) {
  return (
    <div className={s.column}>
      {title && (
        <div className={s.title}>
          {title}
        </div>
      )}
      {children}
    </div>
  )
}

function Footer({ categories, pages }) {
  return (
    <div className={s.root}>
      <Container className={s.container}>
        <Column>
          <Logo dark />
        </Column>

        <Column title="Контакты">
          <a className={s.link} href={`tel:${parsedPhone}`}>
            {phone}
          </a>
          <a className={s.link} href={`mailto:${mail}`}>
            {mail}
          </a> 
        </Column>

        <Column title="Категории">
          <nav className={s.nav}>
            {categories.map(category => (
              <Link
                to={`/category/${category._id}`}
                className={s.link}
                key={category._id}
              >
                {category.label}
              </Link>
            ))}
          </nav>
        </Column>

        <Column title="Магазин">
          {pages.map(page => (
            <Link
              to={`/page/${page._id}`}
              className={s.link}
              key={page._id}
            >
              {page.title}
            </Link>
          ))}
        </Column>
      </Container>
    </div>
  );
}

export default Footer;
