import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import FooterBar from '../containers/FooterBar/FooterBar';
import './variables.css';
import './reset.css';
import s from './Layout.module.css';

export default function Layout({ categories, children, path, className }) {
	return (
		<div className={cx(s.main, className)}>
			{children}

			<FooterBar path={path} categories={categories} />
		</div>
	);
}

Layout.propTypes = {
	children: PropTypes.node.isRequired
};
