import React from 'react';
import CatalogMenu from '../CatalogMenu/CatalogMenu';
import s from './Sidebar.module.css';

function Sidebar({ categories }) {
  return (
    <div className={s.sidebar}>
      <CatalogMenu categories={categories} />
    </div>
  );
}

export default Sidebar;
