import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import isEmpty from 'ramda/src/isEmpty';
import Modal from '..//Modal/Modal';
import ProductMain from './ProductMain/ProductMain';
import Button, { buy } from '..//Button/Button';

import productMainStyles from './ProductMain/ProductMain.module.css';
import BuyForm, { buyFormStyles } from '../BuyForm/BuyForm';
import ProductIcons from '../ProductIcons/ProductIcons';

import timeImg from '../../assets/img/time.svg';
import agesImg from '../../assets/img/player-ages.svg';
import teamImg from '../../assets/img/team.svg';
import SuccessIcon from '../../assets/img/success.inline.svg';
import EyeIcon from '../../assets/img/eye.inline.svg';
import { numberWithSpaces } from '../../helpers/utils';
import { CURRENCY } from '../../constants';
import s from './Product.module.css';

class Product extends Component {
	static propTypes = {
		product: PropTypes.shape({}).isRequired
	};

	state = {
		isProductModalOpen: false,
		isSuccessModalOpen: false,
		isBuyModalOpen: false
	};

	handleBuyClick = () => {
		this.handleOpenBuyModal();
	};

	handleProductClick = () => {
		window.location.pathname = `/products/${this.props.product._id}`;
	};

	handleOpenProductModal = () => this.setState({ isProductModalOpen: true });
	handleCloseProductModal = () => this.setState({ isProductModalOpen: false });

	handleOpenBuyModal = () => this.setState({ isBuyModalOpen: true });
	handleCloseBuyModal = () => this.setState({ isBuyModalOpen: false });

	handleOpenSuccessModal = () => {
		this.setState({ isSuccessModalOpen: true }, () => {
			setTimeout(() => {
				this.handleCloseSuccessModal();
			}, 1500);
		});
	};
	handleCloseSuccessModal = () => {
		this.setState({ isSuccessModalOpen: false });
	};

	handleSuccess = () => {
		this.handleCloseBuyModal();
		this.handleOpenSuccessModal();
	}

	render() {
		const { product } = this.props;
		const { isProductModalOpen, isBuyModalOpen, isSuccessModalOpen } = this.state;
		const {
			title,
			price,
			// salePrice,
			i640: images,
			i400: i200,
			article,
			minDuration,
			maxDuration,
			playerMinAge,
			playerMinCount,
			playerMaxCount,
			playerMaxAge,
			brandColor,
			...rest
		} = product;

		// TODO: add img loader
		const img = !isEmpty(i200)
			? i200[0].url
		 	: !images || isEmpty(images) ? null : images[0].url;

		return (
			<div className={s.root}>
				<div className={s.imgContainer}>
					<img
						alt={title}
						src={img}
						onClick={this.handleProductClick}
						className={s.img}
					/>

					<div className={s.tooltips}>
						<span onClick={this.handleOpenProductModal} className={s.eyeIconContainer}>
							<EyeIcon className={s.eyeIcon} />
						</span>
					</div>
				</div>

				<h3 className={s.title} onClick={this.handleProductClick}>
					{title}
				</h3>

				<div className={s.row}>
					<div className={s.prices}>
						<span className={s.price}>
							{numberWithSpaces(price)}
							<small className={s.currency}>{CURRENCY}</small>
						</span>
						{/* {salePrice && (
							<span className={s.salePrice}>
								{numberWithSpaces(salePrice)}
								<small className={s.currency}>{currency}</small>
							</span>
						)} */}
					</div>
				</div>

				<div className={s.row}>
					<Button onClick={this.handleBuyClick} style={buy}>
						Заказать
					</Button>
				</div>

				<ProductIcons
					// color={brandColor}
					className={cx(s.row, s.bottom)}
					playerMinCount={playerMinCount}
					playerMaxCount={playerMaxCount}
					playerMinAge={playerMinAge}
					playerMaxAge={playerMaxAge}
					minDuration={minDuration}
					maxDuration={maxDuration}
				/>

				{isProductModalOpen && !isBuyModalOpen && (
					<Modal
						className={productMainStyles.modal}
						onRequestClose={this.handleCloseProductModal}
						isOpen={isProductModalOpen}
					>
						<ProductMain
							product={product}
							handleBuyClick={this.handleBuyClick}
						/>
					</Modal>
				)}

				{isBuyModalOpen && (
					<Modal
						className={buyFormStyles.modal}
						closeClassName={buyFormStyles.closeModal}
						onRequestClose={this.handleCloseBuyModal}
						isOpen={isBuyModalOpen}
					>
						<BuyForm
							onSuccess={this.handleSuccess}
							product={{ image: img, article, title, price }}
						/>
					</Modal>
				)}

				{isSuccessModalOpen && (
					<Modal
						className={s.successModal}
						isOpen={isSuccessModalOpen}
					>
						<SuccessIcon height="24" />
						<span className={s.successText}>Спасибо, Ваш заказ взят в работу!</span>
					</Modal>
				)}
			</div>
		);
	}
}

export default Product;
