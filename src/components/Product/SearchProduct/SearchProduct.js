import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { navigate } from '@reach/router';
import { Link } from 'gatsby';
import { numberWithSpaces } from '../../../helpers/utils';
// import cx from 'classnames';
import s from './SearchProduct.module.css';

const currency = 'грн';

class SearchProduct extends Component {
	static propTypes = {
		title: PropTypes.shape({}).isRequired,
		onClick: PropTypes.func,
	};
	static defaultProps = {
		onClick: null,
	}

	handleClick = () => {
		if (this.props.onClick) {
			this.props.onClick();
		}
		navigate(`/products/${this.props.id}`);
	};

	render() {
		const { title, id, price = 0, img } = this.props;

		return (
			<div onClick={this.handleClick} className={s.root}>
				<img className={s.img} alt={''} src={img} />
				<div className={s.text}>
					<div className={s.title}>{title}</div>
					<span className={s.price}>
						{numberWithSpaces(price)} {currency}
					</span>
				</div>
			</div>
		);
	}
}

export default SearchProduct;
