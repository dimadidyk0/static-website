import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'ramda/src/isEmpty';
import path from 'ramda/src/path';
import cx from 'classnames';
import Button, { buy } from '../../Button/Button';

import timeImg from '../../../assets/img/time.svg';
import agesImg from '../../../assets/img/player-ages.svg';
import teamImg from '../../../assets/img/team.svg';
import s from './ProductMain.module.css';

const currency = 'грн';

function numberWithSpaces(num) {
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}

function getRangeString(min = 1, max, sufix) {
	if (!min && !max) {
		return '∞';
	}
	if (min === max) {
		return `${min} ${sufix}`;
	}

	return `${min}${min && max ? ` - ${max}` : '+'} ${sufix}`;
}

class ProductMain extends Component {
	static propTypes = {
		product: PropTypes.shape({}).isRequired
	};

	state = {
		activeImg: undefined
	};

	handlePreviewHover = image => {
		this.setState({ activeImg: image });
	};

	render() {
		const { product } = this.props;
		const {
			title,
			price,
			i640: images = [],
			shortDescription,
			minDuration,
			maxDuration,
			playerMinAge,
			playerMinCount,
			playerMaxCount,
			playerMaxAge
		} = product;

		const firstImg = !images || isEmpty(images) ? null : images[0];
		const activeImg = this.state.activeImg || path(['url'], firstImg);

		const maxPreviewLength = 5;
		const shouldHideExtraPreviews = images && images.length > maxPreviewLength;

		const previewList =
			images && shouldHideExtraPreviews
				? images.slice(0, maxPreviewLength)
				: images;

		return (
			<div className={s.root}>
				<div className={s.left}>
					<div className={s.imgContainer}>
						<img
							alt="title"
							src={activeImg}
							onClick={this.handleOpenModal}
							className={s.img}
						/>
					</div>
					<div className={s.previewList}>
						{previewList.map(({ url: previewImage }, i) => (
							<div key={previewImage} className={s.previewImgContainer}>
								<img
									onMouseEnter={() => this.handlePreviewHover(previewImage)}
									className={s.previewImg}
									alt=""
									src={previewImage}
								/>

								{/* ADd when slider will work */}
								{/* {i + 1 === maxPreviewLength && (
                  <span className={s.previewExtraCount}>
                    +{images.length - maxPreviewLength + 1}
                  </span>
                )} */}
							</div>
						))}
					</div>
				</div>
				<div className={s.right}>
					<div className={cx(s.text, s.text_top)}>
						<h3 className={s.title}>{title}</h3>
					</div>
					<div className={cx(s.text, s.text_bottom)}>
						<div className={s.priceContainer}>
							<div className={s.price}>
								{numberWithSpaces(price)}{' '}
								<small className={s.priceCurrency}>{currency}</small>
							</div>
							<Button
								className={s.buyButton}
								onClick={this.props.handleBuyClick}
								style={buy}
							>
								Заказать
							</Button>
						</div>
					</div>

					<div className={s.text}>
						<div className={s.icons}>
							<div className={s.icon}>
								<img height="18" className={s.iconImg} alt="" src={teamImg} />
								<p className={s.iconText}>
									{getRangeString(playerMinCount, playerMaxCount, 'игроков')}
								</p>
							</div>
							<div className={s.icon}>
								<img height="18" className={s.iconImg} alt="" src={timeImg} />
								<p className={s.iconText}>
									{getRangeString(minDuration, maxDuration, 'минут')}
								</p>
							</div>
							<div className={s.icon}>
								<img height="18" className={s.iconImg} alt="" src={agesImg} />
								<p className={s.iconText}>
									{getRangeString(playerMinAge, playerMaxAge, 'лет')}
								</p>
							</div>
						</div>
					</div>
					{shortDescription && (
						<div className={s.text}>
							<p className={s.description}>{shortDescription}</p>
						</div>
					)}
				</div>
			</div>
		);
	}
}

export default ProductMain;
