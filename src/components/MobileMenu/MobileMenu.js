import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'gatsby';

import ChildrenIcon from '../../assets/img/children.inline.svg';
import FamilyIcon from '../../assets/img/family.inline.svg';
import SexIcon from '../../assets/img/sex.inline.svg';
import TwoIcon from '../../assets/img/two.inline.svg';
import PartyIcon from '../../assets/img/party.inline.svg';
import MustHaveIcon from '../../assets/img/must.inline.svg';

import s from './MobileMenu.module.css';

const imagesMap = {
	'18+': <SexIcon className={s.menuIcon} />,
	Детские: <ChildrenIcon className={s.menuIcon} />,
	'Must have': <MustHaveIcon className={s.menuIcon} />,
	'Для вечеринок': <PartyIcon className={s.menuIcon} />,
	Семейные: <FamilyIcon className={s.menuIcon} />,
	'Для двоих': <TwoIcon className={s.menuIcon} />
};

class MobileMenu extends Component {
	static propTypes = {
		categories: PropTypes.arrayOf(PropTypes.shape()).isRequired,
		onClose: PropTypes.func.isRequired
	};

	render() {
		const { categories, onClose } = this.props;

		return (
			<div className={s.root}>
				{categories.map(category => (
					<Link
						onClick={onClose}
						to={`/category/${category._id}`}
						className={s.menuItem}
						style={{ order: category.order }}
						key={category._id}
					>
						{imagesMap[category.label] || null}
						{category.label}
					</Link>
				))}
			</div>
		);
	}
}

export default MobileMenu;
