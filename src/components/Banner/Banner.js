import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Container from '../Container/Container';
import munchkinImg from './munchkin.png';
import gloom from './gloom.png';
import fallout from './fallout.png';
import banner from './banner.jpg';
import s from './Banner.module.css';

class Banner extends Component {
  static propTypes = {
    value: PropTypes.number.isRequired,
  };

  render() {
    const { value } = this.props;

    return (
      <div className={s.root}>
        <Container className={s.container}>
          {/* <img alt="" src={munchkinImg} className={s.munchkinImg} /> */}
          <img alt="" src={banner} className={s.banner} />
          {/* <img alt="" src={fallout} className={s.fallout} />
          <img alt="" src={"https://desktopgames.com.ua/games/4218/01.png"} className={s.fallout1} /> */}
          {/* <img alt="" src={gloom} className={s.gloom} /> */}

          <div className={s.title}>
            Заботимся о вашем
            <big className={s.titleBig}>досуге</big>
          </div>
        </Container>
      </div>
    );
  }
}

export default Banner;

// import rick from './rick.png';
// import smallworld from './smallworld.png';
// import catan from './catan.png';
// import dixit from './dixit.png';
          {/* <img alt="" src={smallworld} className={s.smallworld} />
          <img alt="" src={catan} className={s.catan} />
          <img alt="" src={dixit} className={s.dixit} /> */}
          {/* <img alt="" src={rick} className={s.rick} /> */}
          {/* <img alt="" src={monopolyImg} className={s.monopolyImg} /> */}