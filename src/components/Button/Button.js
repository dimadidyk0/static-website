import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import s from './Button.module.css';

export const buy = 'buy';

class Button extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
  };

  getClassName = () => {
    const { style, className } = this.props;
    const styleMap = {
      [buy]: s.buy,
    };

    return cx(s.root, className, styleMap[style]);
  };

  render() {
    const { className, icon, children, style, ...rest } = this.props;

    return (
      <button className={this.getClassName()} {...rest}>
        {icon || null}
        {children}
      </button>
    );
  }
}

export default Button;
