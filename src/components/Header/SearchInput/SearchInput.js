import React, { Component } from 'react';
import isEmpty from 'ramda/src/isEmpty';
import s from './SearchInput.module.css';
import SearchIcon from '../../../assets/img/search.inline.svg';
import SearchProduct from '../../Product/SearchProduct/SearchProduct';
import PP from '../../Logo/PP/PP';
import withSearch from '../../../hocs/withSearch';
import { getFirstImage200 } from '../../../helpers/utils';

export class SearchInput extends Component {
  render() {
    const {
      products,
      onInputChange,
      onInputClear,
      isProductsFetching,
      inputValue,
    } = this.props;

    console.log(isProductsFetching, products);
    
    return (
      <div className={s.root}>
        <div className={s.inputContainer}>
          <input
            className={s.input}
            placeholder={'Как же называется эта игра...'}
            onChange={onInputChange}
            value={inputValue}
          />

          <button className={s.searchButton}>
						<SearchIcon
              className={s.searchIcon}
              height="22"
            />
          </button>

          {(isProductsFetching || !isEmpty(products)) && inputValue && (
              <div className={s.searchResult}>
                {isProductsFetching && (
                  <div className={s.loaderContainer}>
                    <PP className={s.loader} />
                  </div>
                )}
                {!isProductsFetching && !isEmpty(products) && products && products.map(({ _id, i200: images, title, price }) => (
                  <SearchProduct
                    id={_id}
                    key={_id}
                    img={getFirstImage200(images).url}
                    title={title}
                    price={price}
                  />
                ))}
              </div>
            )
          }
        </div>
      </div>
    );
  }
}

export default withSearch(SearchInput);
