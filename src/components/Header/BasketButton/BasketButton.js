import React, { Component } from 'react';
import s from './BasketButton.module.css';
import BasketIcon from '../../../assets/img/basket.inline.svg';

export class BasketButton extends Component {
  render() {
    return (
      <div className={s.root}>
        <BasketIcon className={s.icon} />
        <span className={s.text}>Корзина</span>
      </div>
    );
  }
}

export default BasketButton;
