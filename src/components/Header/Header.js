import React, { Component } from 'react';
import Container from '../Container/Container';

import Logo from '../Logo/Logo';
// import BurgerMenuButton from 'components/BurgerMenuButton/BurgerMenuButton';
import HeaderContacts from './HeaderContacts/HeaderContacts';
import Nav from './Nav/Nav';
import SearchInput from './SearchInput/SearchInput';
import BasketButton from './BasketButton/BasketButton';

import s from './Header.module.css';

export class Header extends Component {
	render() {
		const { categories } = this.props;

		return (
			<div className={s.root}>
				<Container className={s.container}>
					{/* <BurgerMenuButton /> */}
					<Logo dark />
					<SearchInput />
					{/* 
					<BasketButton /> */}
					<HeaderContacts />
				</Container>

				<Nav categories={categories} />
			</div>
		);
	}
}

export default Header;
