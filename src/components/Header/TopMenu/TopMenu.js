import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { DESKTOP_NAV } from '../../../assets/data/navigation';
import NavLink from '../../NavLink/NavLink';

import s from './TopMenu.module.css';

export class TopMenu extends Component {
  render() {
    const {
      location: { pathname },
    } = this.props;

    return (
      <nav className={s.root}>
        {DESKTOP_NAV.map(({ link, label }) => (
          <NavLink
            key={link}
            link={link}
            label={label}
            active={pathname.includes(link)}
          />
        ))}
      </nav>
    );
  }
}

export default withRouter(TopMenu);
