import React, { Component } from 'react';
import cx from 'classnames';
import { Link } from 'gatsby';
import Container from '../../Container/Container';

import s from './Nav.module.css';

export class Nav extends Component {
  render() {
    const { categories } = this.props;

    return (
      <Container className={cx(s.container, s.root)}>
          <nav className={s.nav}>
            {categories.map(category => (
              <Link
                to={`/category/${category._id}`}
                className={s.navItem}
                key={category._id}
              >
                {category.label}
              </Link>
            ))}
          </nav>
      </Container>
    );
  }
}

export default Nav;
