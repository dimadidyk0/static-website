import React, { Component } from 'react';
import FavoriteButton from '../../FavoriteButton/FavoriteButton';
import s from './UserControls.module.css';

export class UserControls extends Component {
  render() {
    return (
      <div className={s.root}>
        <FavoriteButton isShort={false} className={s.button} />
        <div className={s.button}>Личный кабинет</div>
      </div>
    );
  }
}

export default UserControls;
