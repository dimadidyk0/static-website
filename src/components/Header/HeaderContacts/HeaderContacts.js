import React, { Component } from 'react';
import { phone, parsedPhone } from '../../../helpers';
import s from './HeaderContacts.module.css';

export class HeaderContacts extends Component {
  render() {

    return (
      <div className={s.root}>
        {/* Сделать иконки = при наведении на иконку отображается контент для телефона и почты */}
        <a className={s.phone} href={`tel:${parsedPhone}`}>
          {phone}
        </a>
        {/* <a className={s.mail} href={`mailto:${mail}`}>
          {mail}
        </a> */}
        <div className={s.schedule}>
          <p>Ежедневно:</p>
          <p>10:00-19:00</p>
        </div>
      </div>
    );
  }
}

export default HeaderContacts;
