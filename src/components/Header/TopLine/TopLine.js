import React, { Component } from 'react';
import Container from '../../Container/Container';
import UserControls from '../UserControls/UserControls';
import TopMenu from '../TopMenu/TopMenu';

import s from './TopLine.module.css';

export class TopLine extends Component {
  render() {
    return (
      <div className={s.root}>
        <Container className={s.container}>
          <TopMenu />
          <UserControls />
        </Container>
      </div>
    );
  }
}

export default TopLine;
