import React, { Component } from 'react';
import PropTypes from 'prop-types';
import s from './Popup.module.css';

class Popup extends Component {
  static propTypes = {
    isOpen: PropTypes.bool,
  };
  static defaultProps = {
    isOpen: true,
  };

  render() {
    const { isOpen, onClose } = this.props;

    if (!isOpen) {
      return null;
    }

    return (
      <div className={s.popup}>
        <div className={s.popup__slider}>
          <img
            className={s.popup__slider_img}
            src="https://play-and-play.s3.eu-central-1.amazonaws.com/7f224806a9e4459ea9aa236473cb597d.jpg"
          />
        </div>
        <button onClick={onClose}>close</button>
      </div>
    );
  }
}

export default Popup;
