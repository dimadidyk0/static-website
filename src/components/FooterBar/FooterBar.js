import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'gatsby';
import MobileMenu from '../MobileMenu/MobileMenu';
import MobileSearchPanel from '../MobileSearchPanel/MobileSearchPanel';
import BurgerMenuIcon from '../../assets/img/burgerMenu.inline.svg';
import SearchIcon from '../../assets/img/search.inline.svg';
import HomeIcon from '../../assets/img/home.inline.svg';
import HeartIcon from '../../assets/img/heart.inline.svg';
import BasketIcon from '../../assets/img/basket.inline.svg';
import { INITIAL_PATH } from '../../constants/location';
import s from './FooterBar.module.css';

const BUTTON_TYPES = {
	CATALOG: 'CATALOG',
	SEARCH: 'SEARCH'
};

class FooterBar extends Component {
	static propTypes = {
		toggleMobileMenu: PropTypes.func.isRequired
	};

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.isMenuOpen !== this.props.isMenuOpen) {
			this.toggleBodyClass();
		}
	}

	toggleBodyClass = () => {
		if (this.props.isMenuOpen) {
			document.querySelector('html').classList.add(s.rootExtender);
			document.body.classList.add(s.rootExtender);
		} else {
			document.querySelector('html').classList.remove(s.rootExtender);
			document.body.classList.remove(s.rootExtender);
		}
	};

	handleCatalogClick = () => {
		this.props.toggleMobileMenu(BUTTON_TYPES.CATALOG);
	};

	handleSearchClick = () => {
		this.props.toggleMobileMenu(BUTTON_TYPES.SEARCH);
	};

	render() {
		const {
			closeMobileMenu,
			isMenuOpen,
			categories,
			path,
			activeTab
		} = this.props;

		const isHomeButtonActive = path === INITIAL_PATH && !isMenuOpen;
		const isCatalogButtonActive =
			activeTab === BUTTON_TYPES.CATALOG && isMenuOpen;
		const isSearchButtonActive =
			activeTab === BUTTON_TYPES.SEARCH && isMenuOpen;

		return (
			<>
				<div
					className={cx(
						s.root,
						isMenuOpen && s.active,
						isSearchButtonActive && s.hidden
					)}
				>
					<div className={cx(s.left, s.side)}>
						<Link onClick={closeMobileMenu} to="/" className={s.button}>
							<HomeIcon
								className={cx(s.icon, isHomeButtonActive && s.active)}
								height="24"
							/>
						</Link>
						<button onClick={this.handleCatalogClick} className={s.button}>
							<BurgerMenuIcon
								className={cx(s.icon, isCatalogButtonActive && s.active)}
								height="24"
							/>
						</button>
					</div>

					<button
						onClick={this.handleSearchClick}
						className={s.searchContainer}
					>
						<SearchIcon height="32" />
					</button>

					<div className={cx(s.right, s.side)}>
						<button className={s.button} disabled>
							<HeartIcon className={s.icon} height="28" />
						</button>
						<button className={s.button} disabled>
							<BasketIcon className={s.icon} height="24" />
						</button>
					</div>
				</div>
				<div className={s.padding} />

				{isCatalogButtonActive && (
					<MobileMenu onClose={closeMobileMenu} categories={categories} />
				)}
				{isSearchButtonActive && (
					<MobileSearchPanel onClose={closeMobileMenu} />
				)}
			</>
		);
	}
}

export default FooterBar;
