import React from 'react';
import cx from 'classnames';
import s from './Container.module.css';

function Container({ children, className }) {
  return <div className={cx(s.root, className)}>{children}</div>;
}

export default Container;
