import React, { Component } from 'react';
import menuSrc from '../../assets/img/menu.svg';
import CategoryMenuItem from './CategoryMenuItem/CategoryMenuItem';
import s from './CatalogMenu.module.css';

class CatalogMenu extends Component {
  render() {
    const { categories } = this.props;
    const title = 'Каталог развлечений';

    const sortedList = [...categories || []].sort((a, b) =>
      a.order - b.order ? -1 : 1,
    );

    return (
      <div className={s.root}>
        <div className={s.title}>
          <img className={s.menuIcon} height="12" src={menuSrc} alt="" />
          {title}
        </div>

        <ul className={s.list}>
          {sortedList.map(({ _id, label, order }) => (
            <CategoryMenuItem
              key={_id}
              link={`/category/${_id}`}
              label={label}
              order={order}
              // childMenu={childMenu}
            />
          ))}
        </ul>
      </div>
    );
  }
}

export default CatalogMenu;
