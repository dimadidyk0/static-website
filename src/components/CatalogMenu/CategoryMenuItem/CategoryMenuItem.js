import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'gatsby';
import s from './CategoryMenuItem.module.css';

class CategoryMenuItem extends Component {
  static propTypes = {
    link: PropTypes.node.isRequired,
    label: PropTypes.node.isRequired,
    subMenu: PropTypes.arrayOf(PropTypes.shape()),
    isChild: PropTypes.bool,
  };
  static defaultProps = {
    isChild: false,
    subMenu: null,
  };

  render() {
    const { link, label, subMenu, isChild } = this.props;

    return (
      <li className={cx(s.root, { [s.child]: isChild })}>
        <Link className={s.link} to={link}>
          {label}
        </Link>

        {subMenu && (
          <ul className={s.childMenu}>
            {subMenu.map(menuItem => (
              <CategoryMenuItem
                key={menuItem.link}
                label={menuItem.label}
                link={menuItem.link}
                isChild
              />
            ))}
          </ul>
        )}
      </li>
    );
  }
}

export default CategoryMenuItem;
