import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'ramda/src/isEmpty';
import Product from '../Product/Product';
import s from './ProductList.module.css';

class ProductList extends Component {
  static propTypes = {
    products: PropTypes.arrayOf(PropTypes.shape({})),
  };
  static defaultProps = {
    products: [],
  };

  render() {
    const { products } = this.props;

    return (
      <div className={s.list}>
        {!isEmpty(products) && products
          ? products.map(product => (
              <Product key={product._id} product={product} />
            ))
          : null}
      </div>
    );
  }
}

export default ProductList;
