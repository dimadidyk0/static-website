import React, { Component } from 'react';
// import Link from '../Link/Link';
import { Link } from 'gatsby';
import LogoTextIcon from '../../assets/img/PandP.svg';
import PP from './PP/PP';
import s from './Logo.module.css';

class Logo extends Component {
	render() {
		const { dark } = this.props;

		return (
			<Link to={'/'} className={s.root}>
				<PP dark={dark} />
				<div className={s.text}>
					<div className={s.logoTextContainer}>
						<img src={LogoTextIcon} alt="" className={s.logoText} />
					</div>
				</div>
			</Link>
		);
	}
}

export default Logo;
