import React, { Component } from 'react';
import cx from 'classnames';
import s from './PP.module.css';

class PPLogo extends Component {
	render() {
		const { className, dark } = this.props;

		return (
			<div className={cx(s.root, className, dark && s.dark)}>
				<div className={s.p1} />
				<div className={s.p1b} />
				<div className={s.p1t} />
				<div className={s.p2} />
				<div className={s.p2b} />
				<div className={s.p2t} />
			</div>
		);
	}
}

export default PPLogo;
