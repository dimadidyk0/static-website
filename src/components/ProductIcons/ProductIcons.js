import React from 'react';
import cx from 'classnames';
import { getRangeString } from '../../helpers/utils';
import timeImg from '../../assets/img/time.svg';
import agesImg from '../../assets/img/player-ages.svg';
import teamImg from '../../assets/img/team.svg';
import s from './ProductIcons.module.css';

function ProductIcons({
	iconClassName,
	className,
	playerMinCount,
	playerMaxCount,
	playerMinAge,
	playerMaxAge,
	minDuration,
	maxDuration,
	_: color
	// handle color in future
}) {
	return (
		<div className={cx(className, s.root)}>
			<span className={cx(s.infoCell, iconClassName)}>
				<span style={{ backgroundColor: color }} className={s.infoCellBg} />
				<img className={s.iconImg} alt="" src={teamImg} />
				<span className={s.iconText}>
					{getRangeString(playerMinCount, playerMaxCount)}
				</span>
			</span>

			<span className={cx(s.infoCell, iconClassName)}>
				<span style={{ backgroundColor: color }} className={s.infoCellBg} />
				<img className={s.iconImg} alt="" src={agesImg} />
				<span className={s.iconText}>
					{getRangeString(playerMinAge, playerMaxAge)}
				</span>
			</span>

			<span className={cx(s.infoCell, iconClassName)}>
				<span style={{ backgroundColor: color }} className={s.infoCellBg} />
				<img className={s.iconImg} alt="" src={timeImg} />
				<span className={s.iconText}>
					{getRangeString(minDuration, maxDuration)}
				</span>
			</span>
		</div>
	);
}
ProductIcons.defaultProps = {
	iconClassName: '',
}

export default ProductIcons;
