import React, { Component } from 'react';
import PropTypes from 'prop-types';
import s from './Scale.module.css';

class Scale extends Component {
  static propTypes = {
    value: PropTypes.number.isRequired,
  };

  render() {
    const { value } = this.props;

    return (
      <div className={s.characteristics__scale}>
        <div className={s.characteristics__scale_item}>
          <span className={s.characteristics__scale_text}>
            {this.props.title}
          </span>
          <div className={s.characteristics__scale_circle}>
            <span className={s.characteristics__scale_circle_fill} />
            <span className={s.characteristics__scale_circle_fill} />
            <span className={s.characteristics__scale_circle_fill} />
            <span />
            <span />
          </div>
        </div>
      </div>
    );
  }
}

export default Scale;
