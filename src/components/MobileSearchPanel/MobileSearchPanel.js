import React, { Component, createRef } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import isEmpty from 'ramda/src/isEmpty';
import SearchProduct from '../Product/SearchProduct/SearchProduct';
import BackIcon from '../../assets/img/left-arrow.inline.svg';
import ClearIcon from '../../assets/img/close.inline.svg';
import { getFirstImage200 } from '../../helpers/utils';
import withSearch from '../../hocs/withSearch';
import PP from '../Logo/PP/PP';

import s from './MobileSearchPanel.module.css';

class MobileSearchPanel extends Component {
	static propTypes = {
		onClose: PropTypes.func.isRequired
	};

	input = createRef();
	root = createRef();

	state = {
		inputValue: '',
		isFetching: false
	};

	componentDidMount() {
		this.focusInput();
	}

	focusInput = () => {
		if (this.input && this.input.current) {
			document.querySelector('html').scrollTop = 0;
			document.body.scrollTop = 0;

			this.input.current.focus();
		}
	};

	handleSubmit = e => {
		e.preventDefault();
		this.props.onClose();
	};

	handleClose = e => {
		e.preventDefault();
		this.props.onClose();
	};

	render() {
		const { onClose, inputValue, isProductsFetching: isFetching, products } = this.props;

		return (
			<div ref={this.root} className={s.root}>
				<form onSubmit={this.handleSubmit} className={s.inputContainer}>
					<button
						onClick={this.handleClose}
						className={cx(s.backButton, s.button)}
					>
						<BackIcon className={s.backIcon} />
					</button>
					<input
						type="search"
						ref={this.input}
						className={s.input}
						placeholder={'Как же называется эта игра...'}
						autoComplete="off"
						onChange={this.props.onInputChange}
						value={inputValue}
					/>
					{inputValue && (
						<button
							onClick={this.props.onInputClear}
							className={cx(s.clearButton, s.button)}
						>
							<ClearIcon className={s.clearIcon} />
						</button>
					)}
				</form>

				{isFetching && (
					<div className={s.loaderContainer}>
						<PP className={s.loader} />
					</div>
				)}
				{!isFetching &&
					products &&
					inputValue &&
					!isEmpty(products) &&
					products.map(({ _id, i200: images, title, price }) => (
						<SearchProduct
							onClick={onClose}
							id={_id}
							key={_id}
							img={getFirstImage200(images).url}
							title={title}
							price={price}
						/>
					))}
				{inputValue && !isFetching && isEmpty(products) && (
					<div className={s.emptyMessage}>
						Игры с названием "{inputValue}" не найдено...
					</div>
				)}
			</div>
		);
	}
}

export default withSearch(MobileSearchPanel);
