import React, { Component } from 'react';
import PropTypes from 'prop-types';
import s from './Input.module.css';

function Input (props) {
	const { name, label, placeholder, value, onChange, required, ...rest } = props;

	return (
		<div className={s.root}>
			<label className={s.label}>
				{label}
				{required && (
					<span className={s.required}> *</span>
				)}
			</label>
			<input
				{...rest}
				required={required}
				placeholder={placeholder}
				onChange={onChange}
				className={s.input}
				name={name}
				value={value}
			/>
		</div>
	);
};

Input.propTypes = {
	name: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	placeholder: PropTypes.string.isRequired,
	value: PropTypes.string,
	onChange: PropTypes.func.isRequired,
	required: PropTypes.bool,
};

Input.defaultProps = {
	value: '',
	required: false,
};

export default Input;
