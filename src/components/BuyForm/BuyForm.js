import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import cx from 'classnames';
import { numberWithSpaces } from '../../helpers/utils';
import { CURRENCY } from '../../constants';
import Input from './Input/Input';
import { sendOrder } from '../../constants/API';
import { ORDER_TYPES } from '../../constants/orderTypes';
import s from './BuyForm.module.css';

class BuyForm extends Component {
	static propTypes = {
		product: PropTypes.shape().isRequired,
		onSuccess: PropTypes.func.isRequired,
	};

	static defaultProps = {};

	state = {
		isFetching: false,
		isSuccess: false,
		form: {
			name: '',
			phone: '',
			email: '',
			promo: '',
		}
	};

	setFieldValue = () => {}

	handleInputChange = e => {
		const { name, value } = e.target;

		this.setState(({ form }) => ({
			form: { ...form, [name]: value }
		}));
	};

	handleSubmit = e => {
		e.preventDefault();

		const { product } = this.props;

		const order = {
			...this.state.form,
			type: ORDER_TYPES.ORDER,
			data: {
				product,
			}
		};

		this.setState({ isFetching: true }, () => {
			sendOrder(order).then(r => {
				if (r.ok) {
					this.setState({ isFetching: false }, () => {
						this.props.onSuccess()
					});
				}
			});
		})
	}

	render() {
		const { product } = this.props;
		const { isFetching } = this.state;

		const inputListData = [
			{
				value: this.state.form.name,
				name: 'name',
				onChange: this.handleInputChange,
				placeholder: 'Как к Вам обращаться?',
				label: 'Имя',
				required: true,
			},
			{
				value: this.state.form.phone,
				name: 'phone',
				onChange: this.handleInputChange,
				placeholder: 'Для подтверждения',
				label: 'Номер телефона',
				required: true,
			},
			{
				value: this.state.form.email,
				name: 'email',
				onChange: this.handleInputChange,
				placeholder: 'Для уведомлений о статусе заказа',
				label: 'Email',
				required: true,
			},
			{
				value: this.state.form.promo,
				name: 'promo',
				onChange: this.handleInputChange,
				placeholder: 'Скидочный купон',
				label: 'Промокод',
				required: false,
			},
		]

		return (
			<form className={s.root} onSubmit={this.handleSubmit}>
				<div className={s.title}>Быстрая покупка</div>

				<img
					className={s.productImg}
					alt={product.title}
					src={product.image}
				/>
				<h3 className={s.productTitle}>
					{product.title}
				</h3>

				<fieldset disabled={isFetching}>
					{inputListData.map(data => <Input key={data.name} {...data} />)}
				</fieldset>

				<span className={s.total}>
					<span>Итого</span>
					<span className={s.totalPrice}>
						{numberWithSpaces(product.price)}
						<small className={s.currency}>{CURRENCY}</small>
					</span>
				</span>

				<button disabled={isFetching} className={s.sendBtn}>Отправить</button>
			</form>
		);
	}
}

export const buyFormStyles = s;
export default BuyForm;
