import { connect } from 'react-redux';
import {
	isMobileMenuOpenSelector,
	activeMobileTabSelector
} from '../../../state/reducers/ui';
import {
	toggleMobileMenu,
	openMobileMenu,
	closeMobileMenu
} from '../../../state/actions/ui';

import FooterBar from '../../FooterBar/FooterBar';

const mapStateToProps = state => ({
	isMenuOpen: isMobileMenuOpenSelector(state),
	activeTab: activeMobileTabSelector(state)
});

const mapDispatchToProps = {
	toggleMobileMenu,
	openMobileMenu,
	closeMobileMenu
};

export default connect(mapStateToProps, mapDispatchToProps)(FooterBar);
