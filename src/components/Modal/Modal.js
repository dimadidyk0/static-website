import React, { Component } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';
import s from './Modal.module.css';

class Modal extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    isOpen: PropTypes.bool,
    closeClassName: PropTypes.string,
  };

  static defaultProps = {
    isOpen: false,
    closeClassName: '',
  };

  render() {
    const { isOpen, children, className, closeClassName, onRequestClose, ...rest } = this.props;

    return (
      <ReactModal
        {...rest}
        isOpen={isOpen}
        className={cx(s.modal, className)}
        onRequestClose={onRequestClose}
        overlayClassName={s.overlay}>
        {children}
        <span className={cx(s.close, closeClassName)} onClick={onRequestClose}>
          ×
        </span>
      </ReactModal>
    );
  }
}

export default Modal;
