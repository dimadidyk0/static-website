import React from 'react';
import ReactMarkdown from 'react-markdown';

import Layout from '../../components/Layout/Layout';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Sidebar from '../../components/Sidebar/Sidebar';
import Container from '../../components/Container/Container';
import ProductList from '../../components/ProductList/ProductList';
// import SEO from "../components/seo"

// import Loader from 'components/Loader/Loader';
// import withHomeProducts from 'hocs/withHomeProducts';
// import Footer from 'components/Footer/Footer';
// import FooterBar from 'components/FooterBar/FooterBar';
import bg from './bg.jpg';
import s from './Page.module.css';

const Page = ({
	pageContext: { categories, page, pages },
	path
}) => {
	const { title, content } = page;

	console.log(page);

	return (
		<>
			<Header categories={categories} />

			<Layout path={path} categories={categories} className={s.layout}>
				<div className={s.imgContainer}>
					<img alt="" className={s.img} src={bg} />
				</div>
				<Container className={s.container}>
					<div className={s.root}>
						<h1 className={s.title}>{title}</h1>

						<ReactMarkdown source={content} />
					</div>
					{/* <SEO title="Home" /> */}
				</Container>
			</Layout>

			<Footer categories={categories} pages={pages} />
		</>
	);
};

export default Page;
