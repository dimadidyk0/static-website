import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';

import ProductIcons from '../../components/ProductIcons/ProductIcons';

import Popup from '../../components/Popup/Popup';
import Scale from '../../components/Scale/Scale';
import Layout from '../../components/Layout/Layout';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Sidebar from '../../components/Sidebar/Sidebar';
import Container from '../../components/Container/Container';

import Modal from '../../components/Modal/Modal';
import BuyForm, { buyFormStyles } from '../../components/BuyForm/BuyForm';

import BasketIcon from '../../assets/img/basket.inline.svg';
import SuccessIcon from '../../assets/img/success.inline.svg';

import s from './Product.module.css';

class Product extends Component {
	state = {
		currentImage: null,
		isSuccessModalOpen: false,
		isBuyModalOpen: false
	};

	handleOpenBuyModal = () => {
		this.setState({ isBuyModalOpen: true });
	};
	handleCloseBuyModal = () => {
		this.setState({ isBuyModalOpen: false });
	};

	handleOpenSuccessModal = () => {
		this.setState({ isSuccessModalOpen: true }, () => {
			setTimeout(() => {
				this.handleCloseSuccessModal();
			}, 1500);
		});
	};
	handleCloseSuccessModal = () => {
		this.setState({ isSuccessModalOpen: false });
	};

	handleSuccess = () => {
		this.handleCloseBuyModal();
		this.handleOpenSuccessModal();
	}

	setCurrentImage = image => {
		this.setState({ currentImage: image });
	};

	render() {
		const { currentImage, isBuyModalOpen, isSuccessModalOpen } = this.state;
		const {
			pageContext: { product: game, categories, pages },
			path
		} = this.props;

		const {
			title,
			playerMinCount,
			playerMaxCount,
			playerMinAge,
			playerMaxAge,
			minDuration,
			maxDuration,
			i640: images,
			description,
			price,
			article,
			brandColor,
			equipment,
			...rest
			// subtitle = 'Test subtitle'
		} = game;

		const activeImage = currentImage || images[0].url;

		return (
			<>
			<Header categories={categories} />

				<Layout path={path} categories={categories}>
					<Container className={s.container}>
						{/* <Sidebar categories={categories} /> */}

						<div className={s.main}>

							<div className={s.game}>
								<div className={s.left}>
									<div className={s.slider}>
										<img className={s.sliderMain} src={activeImage} />

										<div className={s.sliderCatalog}>
											{images.map(({ url }) => (
												<img
													className={s.sliderImageItem}
													onClick={() => this.setCurrentImage(url)}
													src={url}
												/>
											))}
										</div>
									</div>
								</div>

								<div className={s.right}>
									<div className={s.top}>
										<h1 className={s.title}>{title}</h1>
										{/* <p className={s.subtitle}>{subtitle}</p> */}

										<ProductIcons
											iconClassName={s.icon}
											color={brandColor}
											className={s.icons}
											playerMinCount={playerMinCount}
											playerMaxCount={playerMaxCount}
											playerMinAge={playerMinAge}
											playerMaxAge={playerMaxAge}
											minDuration={minDuration}
											maxDuration={maxDuration}
										/>
									</div>

									<div className={s.order}>
										<div className={s.price}>{price} грн</div>
										<button className={s.orderBtn} onClick={this.handleOpenBuyModal}>
											<BasketIcon
												className={s.orderIcon}
												height="18"
												width="18"
											/>
											Купить
										</button>
									</div>

									<div className={s.textBlock}>
										<div className={s.textTitle}>Описание</div>
										<div className={s.textContent}>
											{description ? <ReactMarkdown source={description} /> : 'Мы пока не нашли слов, чтоб описать на сколько это крутая игра...'}
										</div>
									</div>

									{
										equipment ? (
											<div className={s.textBlock}>
												<div className={s.textTitle}>В коробке</div>
												<div className={s.textContent}>
													<ReactMarkdown source={equipment} />
												</div>
											</div>
										) : null
									}
								</div>

								{/* <div className={s.description}>{description}</div> */}
							</div>
						</div>
					</Container>

					{isBuyModalOpen && (
						<Modal
							className={buyFormStyles.modal}
							closeClassName={buyFormStyles.closeModal}
							onRequestClose={this.handleCloseBuyModal}
							isOpen={isBuyModalOpen}
						>
							<BuyForm
								onSuccess={this.handleSuccess}
								product={{ image: activeImage, article, title, price }}
							/>
						</Modal>
					)}

					{isSuccessModalOpen && (
						<Modal
							className={s.successModal}
							isOpen={isSuccessModalOpen}
						>
							<SuccessIcon height="24" />
							<span className={s.successText}>Спасибо, Ваш заказ взят в работу!</span>
						</Modal>
					)}
				</Layout>

				<Footer categories={categories} pages={pages} />
			</>
		);
	}
}

export default Product;
