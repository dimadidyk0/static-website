import React from 'react';
import { Link } from 'gatsby';

import Layout from '../../components/Layout/Layout';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Sidebar from '../../components/Sidebar/Sidebar';
import Container from '../../components/Container/Container';
import ProductList from '../../components/ProductList/ProductList';
// import SEO from "../components/seo"

// import Loader from 'components/Loader/Loader';
// import withHomeProducts from 'hocs/withHomeProducts';
// import Footer from 'components/Footer/Footer';
// import FooterBar from 'components/FooterBar/FooterBar';
import s from './Category.module.css';

const Category = ({
	pageContext: { category, products, categories, pages },
	path
}) => {
	return (
		<>
			<Header categories={categories} />

			<Layout path={path} categories={categories}>
				<Container className={s.container}>
					{/* <Sidebar categories={categories} /> */}

					<div className={s.root}>
						<ProductList products={products} />
					</div>
					{/* <SEO title="Home" /> */}
				</Container>
			</Layout>

			<Footer categories={categories} pages={pages} />
		</>
	);
};

export default Category;
