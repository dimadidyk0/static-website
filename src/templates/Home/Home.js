import React from 'react';
import { Link } from 'gatsby';
import { connect } from 'react-redux';

import Layout from '../../components/Layout/Layout';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Sidebar from '../../components/Sidebar/Sidebar';
import Container from '../../components/Container/Container';
import ProductList from '../../components/ProductList/ProductList';
import Banner from '../../components/Banner/Banner';
import SEO from '../../components/seo';

// import Loader from 'components/Loader/Loader';
// import withHomeProducts from 'hocs/withHomeProducts';
// import Footer from 'components/Footer/Footer';
import s from './Home.module.css';

const Home = ({ pageContext: { products, categories, pages }, path }) => {

	return (
		<>
			<Header categories={categories} />
			<Layout path={path} categories={categories}>
				<Banner />

				<Container className={s.container}>
					{/* <Sidebar categories={categories} /> */}

					<div className={s.root}>
						<ProductList products={products} />
					</div>

					<SEO
						title="Настольные игры - Play and Play | Купить настольную игру в Киеве. Интернет-магазин настольных игр - play-and-play. Доставка: Киев, по Украине."
						description="Настольные игры купить в интернет магазине Play and Play. Доставка игр по Киеву и всей Украине. Огромный выбор настольных игр. Звоните (097)390-53-45"
						keywords="настолка, настолки, настольные игры, купить настольную игру киев, игры настольные, украина, доставка, купить игры, доставка бесплатная, магазин игр, интернет магазин настольных игр, Dixit Ticket to Ride"
					/>
				</Container>
			</Layout>
			<Footer categories={categories} pages={pages} />
		</>
	);
};

export default Home;
