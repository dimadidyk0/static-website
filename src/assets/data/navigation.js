export const CATEGORY = 'category';

export const LOGIN_URL = '/login';
export const LOGOUT_URL = '/logout';
export const REGISTER_URL = '/register';

export const DESKTOP_NAV = [
  // {
  //   label: 'Акции',
  //   link: '/bonus',
  // },
  // {
  //   label: 'FAQ',
  //   link: '/faq',
  // },
  {
    label: 'Самовывоз',
    link: '/self',
  },
  {
    label: 'Доставка',
    link: '/delivery',
  },
  {
    label: 'Контакты',
    link: '/contacts',
  },
  // {
  //   label: 'LogIn',
  //   link: LOGIN_URL,
  // },
];
