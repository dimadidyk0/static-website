import { LOAD_PRODUCTS, START, SUCCESS, ERROR } from '../constants/actionTypes';

export const getProductsStart = payload => ({
  type: `${LOAD_PRODUCTS}${START}`,
  payload,
});
export const getProductsSuccess = payload => ({
  type: `${LOAD_PRODUCTS}${SUCCESS}`,
  payload,
});
export const getProductsError = payload => ({
  type: `${LOAD_PRODUCTS}${ERROR}`,
  payload,
});
