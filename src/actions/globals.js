import { SET_GLOBALS_LANGUAGE } from '../constants';

export const setGlobalsLanguage = payload => ({
  type: SET_GLOBALS_LANGUAGE,
  payload,
});
