export function debounce(f, ms) {
	let timeoutId = null;

	return function() {
		clearTimeout(timeoutId);

		timeoutId = setTimeout(() => {
			f.apply(this, arguments);
		}, ms);
	};
}

export function numberWithSpaces(num) {
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}

export function getRangeString(min = 1, max, sufix = '') {
	if (!min && !max) {
		return '∞';
	}
	if (min === max) {
		return `${min}${sufix ? ` ${sufix}` : ''}`;
	}

	return `${min}${min && max ? `-${max}` : '+'}${sufix ? ` ${sufix}` : ''}`;
}


export function getFirstImage200(images) {
	if (!images) {
		return {};
	};

	const map = [...images].sort((a, b) => {
		if (a.name > b.name) return 1;
		if (a.name < b.name) return -1;

		return 0;
	});

	return map[0] || {};
}
