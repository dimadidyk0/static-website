import path from 'ramda/src/path';
import {
	TOGGLE_MOBILE_MENU,
	OPEN_MOBILE_MENU,
	CLOSE_MOBILE_MENU
} from '../constants';

const initialTab = null;

const initialState = {
	mobileBar: {
		activeTab: initialTab,
		isOpen: false
	}
};

const uiReducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case TOGGLE_MOBILE_MENU:
			return {
				...state,
				mobileBar: {
					isOpen: state.mobileBar.activeTab !== payload,
					activeTab:
						state.mobileBar.activeTab !== payload ? payload : initialTab
				}
			};
		case OPEN_MOBILE_MENU:
			return {
				...state,
				mobileBar: {
					activeTab: payload,
					isOpen: true
				}
			};
		case CLOSE_MOBILE_MENU:
			return {
				...state,
				mobileBar: {
					activeTab: initialTab,
					isOpen: false
				}
			};

		default:
			return state;
	}
};

export const isMobileMenuOpenSelector = state =>
	path(['ui', 'mobileBar', 'isOpen'], state);
export const activeMobileTabSelector = state =>
	path(['ui', 'mobileBar', 'activeTab'], state);

export default uiReducer;
