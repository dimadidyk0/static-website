import {
	TOGGLE_MOBILE_MENU,
	OPEN_MOBILE_MENU,
	CLOSE_MOBILE_MENU
} from '../constants';

export const toggleMobileMenu = payload => ({
	type: TOGGLE_MOBILE_MENU,
	payload
});

export const openMobileMenu = payload => ({
	type: OPEN_MOBILE_MENU,
	payload
});

export const closeMobileMenu = () => ({
	type: CLOSE_MOBILE_MENU
});
