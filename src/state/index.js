import { combineReducers } from 'redux';
import app from './app';
import ui from './reducers/ui';

export default combineReducers({ app, ui });
