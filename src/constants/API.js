export const API_HOST = 'https://calm-hamlet-90481.herokuapp.com';

export const PRODUCTS_URL = `${API_HOST}/products`;
export const ORDERS_URL = `${API_HOST}/orders`;

export const sendOrder = orderData => 
  fetch(
    `${ORDERS_URL}`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(orderData) // body data type must match "Content-Type" header
    }
  );
