const subscribers = require('./subscribers.json');
const subscriptions = require('./t.json');

const result = [];

subscriptions.forEach(el => {
	if (!subscribers.includes(el)) {
		result.push(el);
	}
});

console.log('\n\n\n');
console.log(JSON.stringify(result));
console.log('\n\n\n');

return result;
