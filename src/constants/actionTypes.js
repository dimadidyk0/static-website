export const START = '_START';
export const SUCCESS = '_SUCCESS';
export const ERROR = '_ERROR';

export const SET_GLOBALS_LANGUAGE = 'SET_GLOBALS_LANGUAGE';
export const LOAD_PRODUCTS = 'LOAD_PRODUCTS';
